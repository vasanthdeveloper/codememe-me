/*
 *  Vite run control for codememe.me.
 *  Created On 23 July 2021
 */

import autoprefixer from 'autoprefixer'
import postCSSImport from 'postcss-import'
import inlineSVG from 'posthtml-inline-svg'
import modules from 'posthtml-modules'
import tailwindcss from 'tailwindcss'
import { defineConfig } from 'vite'
import { minifyHtml } from 'vite-plugin-html'
import { posthtmlPlugin } from 'vite-plugin-posthtml'

export default ({ mode }) =>
    defineConfig({
        build: { emptyOutDir: true, outDir: '../dist' },
        plugins: [
            // run PostHTML to construct the dist
            posthtmlPlugin({
                plugins: [
                    // render other HTML files as modules
                    modules({
                        root: 'src',
                    }),

                    // inline SVGs wherever required
                    // straight into HTML
                    inlineSVG({
                        cwd: 'src',
                        tag: 'vector',
                        attr: 'src',
                    }),
                ],
            }),

            mode == 'production' ? minifyHtml() : null,
        ],
        css: {
            postcss: {
                plugins: [postCSSImport(), tailwindcss(), autoprefixer()],
            },
        },
    })
