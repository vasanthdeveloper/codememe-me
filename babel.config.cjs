/* eslint-env node */
/*
 *  Babel transpiler run control for codememe.me.
 *  Created On 24 July 2021
 */

module.exports = {
    plugins: [require('@babel/plugin-syntax-top-level-await')],
}
