import { VercelRequest, VercelResponse } from '@vercel/node'
import axios from 'axios'
import fs from 'fs/promises'
import path from 'path'

// read the text file in the root of this project
// parse the newline separated subreddit slugs and
// convert them into an array of strings
const getSubreddits = async (): Promise<string[]> => {
    let nsv = (await fs.readFile(
        path.join(__dirname, '..', 'subreddits.txt'),
        'utf-8',
    )) as any

    nsv = nsv
        .split('\n')
        .filter(name => Boolean(name))
        .map(name => name.trim())

    return nsv
}

// get memes for a given subreddit
const getMemes = async (
    subreddit: string,
): Promise<Array<{ title: string; url: string }>> => {
    // send an HTTP GET request to Reddit's API
    // to fetch all posts in a given subreddit
    let {
        data: {
            data: { children: memes },
        },
    } = await axios({
        method: 'GET',
        url: `https://reddit.com/r/${subreddit}.json`,
    })

    // map through all memes and get the data obj
    memes = memes.map(meme => meme.data)

    // filter results with images
    // return memes that are true, then ! it.  return only false ones
    memes = memes.filter(meme => !meme.is_video)

    // filter out memes without images
    memes = memes.filter(meme => Boolean(meme.url))

    // filter out memes with adult content
    memes = memes.filter(meme => !meme.over_18)

    // filter out memes with external images
    memes = memes.filter(meme => meme.domain == 'i.redd.it')

    // filter the title and image URL and replace
    // the entire meme object with only the required fields
    memes = memes.map(meme => {
        return {
            title: meme.title,
            image: meme.url,
        }
    })

    return memes
}

export default async (
    req: VercelRequest,
    res: VercelResponse,
): Promise<any> => {
    // read the subreddits.txt file and get an array
    const subreddits = await getSubreddits()

    // pick a random subreddit
    const subreddit = subreddits[Math.floor(Math.random() * subreddits.length)]

    // get memes from the picked subreddit
    const memes = await getMemes(subreddit)

    res.json({
        subreddit: subreddit,
        memes,
    })
}
